const gulp = require('gulp'),
      sass = require('gulp-sass'),
      cssnano = require('gulp-cssnano'),
      autoprefixer = require('gulp-autoprefixer'),
      rename = require('gulp-rename'),
      babel = require('gulp-babel'),
      del = require('del'),
      imagemin = require('gulp-imagemin');

const path = {
  src: './src/',
  dist: './dist/'
};
/**
 * Build all project assets and dependencies to deployment
 */
gulp.task('default', ['build:css', 'build:js', 'build:img', 'build:font'], () =>
  gulp.src(`${path.src}**/*.html`)
   .pipe(gulp.dest(`${path.dist}`))
);
/**
 * Clear dist content
 * @requires del {@link https://www.npmjs.com/package/del}
 */
gulp.task('clear', () =>
  del([`${path.dist}`])
);
/**
 * Copy all font assets to dist folder
 */
gulp.task('build:font', () =>
  gulp.src(`${path.src}fonts/*`)
    .pipe(gulp.dest(`${path.dist}fonts`))
);
/**
 * Compress images for better performance
 * @requires gulp-imagemin {@link https://www.npmjs.com/package/gulp-imagemin}
 */
gulp.task('build:img', () =>
  gulp.src(`${path.src}img/*`)
    .pipe(imagemin())
    .pipe(gulp.dest(`${path.dist}img`))
);
/**
 * Transpile es6 js files into es5 ones
 * @requires gulp-babel {@link https://www.npmjs.com/package/gulp-babel}
 */
gulp.task('build:js', () =>
  gulp.src(`${path.src}js/neon.js`)
    .pipe(babel({
      presets: ['es2015']
    }))
    .pipe(rename('neon.min.js'))
    .pipe(gulp.dest(`${path.dist}js`))
);
/**
 * Transpile .scss files into .css files
 * @requires gulp-sass {@link https://www.npmjs.com/package/gulp-sass}
 * @requires gulp-autoprefixer {@link https://www.npmjs.com/package/gulp-autoprefixer}
 * @requires gulp-cssnano {@link https://www.npmjs.com/package/gulp-cssnano}
 */
gulp.task('build:css', () =>
  gulp.src(`${path.src}sass/neon.scss`)
    .pipe(sass().on('error', sass.logError))
    .pipe(autoprefixer({
      browsers: ['last 2 versions'],
      cascade: false
    }))
    .pipe(cssnano())
    .pipe(rename('neon.min.css'))
    .pipe(gulp.dest(`${path.dist}css`))
);
