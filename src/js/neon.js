/**
 * Vanilla JS EcmaScript 2015 Carousel
 * Simple iterator through html node list
 *
 * @class Carousel
 */
const Carousel = class Carousel {
  /**
   * @constructor Carousel
   * @param {Object} element
   */
  constructor(element) {
    this.carousel = element;
    this.carouselCards = [...element.querySelectorAll(`[data-${Carousel.constrain}-card]`)];
    this.carouselCurrentCard = 0;
    this.carouselPreviousArrow = element.querySelector(`[data-${Carousel.constrain}-previous]`);
    this.carouselNextArrow = element.querySelector(`[data-${Carousel.constrain}-next]`);

    this.init();
  }
  /**
   * Enable a slide based on given index, then animate it based on given animation
   * @param {number} index
   * @param {string} animation
   */
  enableSlide(index, animation) {
    this.carouselCards[index].classList.remove('-state-inactive', '-fade-from-left', '-fade-to-left', '-fade-from-right', '-fade-to-right');
    this.carouselCards[index].classList.add('-state-active', animation);
  }
  /**
   * Disable a slide based on given index, then animate it based on given animation
   * @param {number} index
   * @param {string} animation
   */
  disableSlide(index, animation) {
    this.carouselCards[index].classList.remove('-state-active', '-fade-from-left', '-fade-to-left', '-fade-from-right', '-fade-to-right');
    this.carouselCards[index].classList.add('-state-inactive', animation);
  }
  /**
   * Navigate to previous slide on carouselCards index
   * @param {number} current
   */
  previousSlide(current) {
    this.disableSlide(current, '-fade-to-right');
    current--;
    /**
     * If it's the first position, return to the last slide
     */
    if(current < 0) {
      this.carouselCurrentCard = (this.carouselCards.length - 1);
      this.enableSlide(this.carouselCurrentCard, '-fade-from-left');
      return;
    }

    this.carouselCurrentCard = current;
    this.enableSlide(this.carouselCurrentCard, '-fade-from-left');
  }
  /**
   * Navigate to next slide on carouselCards index
   * @param {number} current
   */
  nextSlide(current) {
    this.disableSlide(current, '-fade-to-left');
    current++;
    /**
     * If it's the last position, return to the first slide
     */
    if(current >= this.carouselCards.length) {
      this.carouselCurrentCard = 0;
      this.enableSlide(this.carouselCurrentCard, '-fade-from-right');
      return;
    }

    this.carouselCurrentCard = current;
    this.enableSlide(this.carouselCurrentCard, '-fade-from-right');
  }
  /**
   * Event liesteners wrapper
   */
  observe() {
    this.carouselPreviousArrow.addEventListener('click', () => this.previousSlide(this.carouselCurrentCard));

    this.carouselNextArrow.addEventListener('click', () => this.nextSlide(this.carouselCurrentCard));
  }
  /**
   * Setup the object
   */
  init() {
    this.observe();
  }
  /**
   * Default data-attribute namespace
   * @public
   * @return {string}
   */
  static get constrain() {
    return 'carousel';
  }
  /**
   * @public
   * @param {object} content
   */
  static init(elementInner = document) {
    [...elementInner.querySelectorAll(`[data-${Carousel.constrain}]`)].forEach(element => new Carousel(element));
  }
};

Carousel.init();
