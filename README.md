![neon](https://lh6.googleusercontent.com/nEk1FRoupK8QQhElPGjUnDbx-NXdAmV_wpu36484zf5m_qtw5GiZQgII2DmoWl9kQ83_sSnmPwiA-zwoNVT7rXMyuq0jKKvz1o1wm4hCp6NJD9YY3OINMthD3gU8YqUrn5wbjdTv)

# Neon Front End application test
Hi! That's my application test for the Front End position at @banconeon, and here i explain some of my choices of architecture.

## Running the project
Simply go to the `neon` directory and run:
```javascript
npm i && gulp
```
A `dist` folder will be generated and you can open `index.html` file to run it locally.

## Stack
I decided to bring the stack that i'm used to, it's highly performatic and kinda common:
- Good 'ol semantic html5
- SASS as CSS pre processor
- GULP as task runner
- Vanilla JavaScript in EcmaScript2015 (ES6) notation

## Architecture
Before we start, is important to note that the project was *entirely build from scratch*, without any kind of boilerplate or something.

Said that, the styles structure was based mostly in Atomic Design, with some variants of BEM (such as notations on molecules for non-deep specificity) and RSCSS (seen on modifiers syntax based on trumpets). Despite the small portion of JS needed, it follows the best practices on ES6 syntax (transpiled via Babel on Gulp) with OO.

I hope you enjoy the project and call me to work with you, guys!

'Til soon!
